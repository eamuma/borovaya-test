const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');

module.exports = {
	entry: './assets/entry.js',
	output: {
		filename: 'scripts.js',
		path: path.resolve(__dirname, 'public')
	},
	plugins: [new MiniCssExtractPlugin({
		filename: 'styles.css',
	})],
	module: {
		rules: [
		{
			test: /\.js$/,
			exclude: /(node_modules|bower_components)/,
			use: {
				loader: 'babel-loader',
				options: {
					presets: ['@babel/preset-env']
				}
			}
		},
		{
			test: /\.s[ac]ss$/i,
			use: [MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader', 'resolve-url-loader', 'sass-loader']
		}
		]
	},

	devtool: 'eval'
};