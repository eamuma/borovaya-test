$(function () {
	$('.catalog__brands__menu-ul').on('click', 'a', function(e) {
		e.preventDefault();
		let $products = $('.products__product');

		$('.brand').removeClass('active');
		$(this).addClass('active');

		if ($(this).attr('data-brand') === 'all') {
			$products.show();
		} else {
			$products.hide();

			let brand = $(this).attr('data-brand');
			$('.products').find(`[data-brand*="${brand}"]`).show();
		}
	});
	$('.brand.active').trigger('click');
});